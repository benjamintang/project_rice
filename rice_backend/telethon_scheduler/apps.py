from django.apps import AppConfig


class TelethonSchedulerConfig(AppConfig):
    name = 'telethon_scheduler'
